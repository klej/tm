angular.module('tsApp', ['ui.router'])
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/main');
        $stateProvider
            .state('default', {
                abstract: true,
                views: {
                    'header': {
                        templateUrl: 'templates/header.html'
                    },
                    'container': {
                        template: '<ui-view></ui-view>'
                    }
                }
            })
            .state('default.main', {
                url: '/main',
                templateUrl: 'templates/main.html',
                controller: 'MainController',
                controllerAs: 'vm'
            })
            .state('default.new', {
                url: '/new',
                templateUrl: 'templates/new.html',
                controller: 'NewNotificationController',
                controllerAs: 'vm'
            })

    })

    .controller('MainController', mainController)
    .controller('NewNotificationController', newNotificationController)

    .directive('notifications', notifications)

    .factory('NotificationsService', notificationsService);

/**
 * @ngdoc controller
 * @name tsApp.controller:MainController
 * @description
 * Main controller of application
 */
function mainController(NotificationsService) {
    var vm = this;

    vm.loadNotifications = function() {
        NotificationsService.loadNotifications();
    };
}

/**
 * @ngdoc controller
 * @name tsApp.controller:NewNotificationController
 * @description
 * Creates a new notification
 *
 * @param {object}  NotificationsService Notifications service
 * @param {object}  $state Ui router state
 */
function newNotificationController(NotificationsService, $state) {
    var vm = this;

    vm.categories = ["info", "warning", "error"];
    vm.types = [
        {name: "none", value: "note"},
        {name: "confirm", value: "ok_confirm"},
        {name: "confirm with cancel", value: "ok_cancel_confirm"}
    ];
    vm.notification = {
        category: vm.categories[0],
        type: vm.types[0].value
    };
    vm.createNotification = function() {
        NotificationsService.addNotification(vm.notification);
        $state.go('default.main');
    };
}

/**
 * @ngdoc directive
 * @name tsApp.directive:notifications
 * @restrict A
 * @description
 * Block that contains notifications
 */
function notifications() {
    return {
        restrict: 'A',
        templateUrl: 'templates/notifications.html',
        controller: notificationsController,
        controllerAs: 'vm'
    };
}

/**
 * @ngdoc service
 * @name tsApp.service:NotificationsService
 * @description
 * The service working with notifications
 *
 * @param {function}  $timeout Timeout function
 * @param {object}  $http Http service
 */
function notificationsService($timeout, $http) {
    /**
     * @ngdoc property
     * @name notifications
     * @propertyOf tsApp.NotificationsService
     * @description
     * Array of notifications
     */
    var notifications = [];
    var defaultCount = 5,
        viewCount = defaultCount;

    var service = {
        addNotification: addNotification,
        getNotifications: getNotifications,
        closeNotification: removeNotification,
        getNotificationsSize: getNotificationsSize,
        loadNotifications: loadNotifications,
        sendNotificationResponse: sendNotificationResponse
    };
    return service;

    /**
     * @ngdoc method
     * @name addNotification
     * @methodOf tsApp.NotificationsService
     * @description
     * Adds new notification to notifications array
     *
     * @param {object} notification New notification object
     */
    function addNotification(notification) {
        var notificationTime = 90000;

        var current = angular.copy(notification);

        current.id = current.id || Date.now();

        notifications.push(current);

        if(current.type == 'note') {
            $timeout(function() {
                removeNotification(current.id);
            }, notificationTime);
        }
    }

    /**
     * @ngdoc method
     * @name getNotifications
     * @methodOf tsApp.NotificationsService
     * @description
     * Get visible notifications
     *
     * @param {number=} addCount Increment visible notifications
     * @returns {object} notifications and flag to identify if we should group the notifications
     */
    function getNotifications(addCount) {
        if(addCount) {
            viewCount += addCount;
        }

        var response = {
            sliceFlag: false,
            array: notifications
        };

        if(notifications.length > viewCount) {
            response.array = response.array.slice(-(viewCount - 1));
            response.sliceFlag = true;
        }

        addClass(response.array);

        return response;
    }

    function addClass(notifications) {
        var categories = {
            info: "alert-info",
            warning: "alert-warning",
            error: "alert-danger"
        };

        notifications.forEach(function(notification) {
            notification.klass = categories[notification.category];
        });
    }

    /**
     * @ngdoc method
     * @name removeNotification
     * @methodOf tsApp.NotificationsService
     * @description
     * Removes notification with the specified id
     *
     * @param {number} id Notification id
     */
    function removeNotification(id) {
        notifications.forEach(function(item, index) {
            if(item.id == id) {
                notifications.splice(index, 1);
                if(viewCount > defaultCount) {
                    viewCount--;
                }
            }
        });
    }

    /**
     * @ngdoc method
     * @name getNotificationsSize
     * @methodOf tsApp.NotificationsService
     * @description
     * Gets size of notifications array
     *
     * @returns {number} Size
     */
    function getNotificationsSize() {
        return notifications.length;
    }

    /**
     * @ngdoc method
     * @name loadNotifications
     * @methodOf tsApp.NotificationsService
     * @description
     * Loads notifications from external resource
     */
    function loadNotifications() {
        var myUrl = "http://www.json-generator.com/api/json/get/cgnqJBpIpu",
            defaultUrl = "/api/notification/list";

        $http.get(myUrl).then(function(response) {
            response.data.forEach(function(notification) {
                addNotification(notification);
            });
        });
    }

    /**
     * @ngdoc method
     * @name sendNotificationResponse
     * @methodOf tsApp.NotificationsService
     * @description
     * Sends response for notification
     *
     * @param {object} notification Notification object
     * @param {number} response Response parameter [0,1]
     */
    function sendNotificationResponse(notification, response) {
        var url = "/api/notification/confirm",
            data = {
                id: notification.id,
                from: notification.from,
                result: response
            };

        $http.post(url, data);

        removeNotification(notification.id);
    }
}

/**
 * @ngdoc controller
 * @description
 * Main controller of notifications directive
 *
 * @param {object}  $scope Scope
 * @param {object}  NotificationsService Notifications service
 */
function notificationsController($scope, NotificationsService) {
    var vm = this;
    var addCount = 0;

    $scope.$watch(function() {
        return NotificationsService.getNotificationsSize();
    }, showNotifications);

    function showNotifications() {
        var result = NotificationsService.getNotifications(addCount);
        addCount = 0;

        vm.notifications = result.array;
        vm.sliceFlag = result.sliceFlag;
    }

    vm.loadPrevious = function() {
        addCount = 5;
        showNotifications();
    };

    vm.close = function(id) {
        NotificationsService.closeNotification(id);
    };

    vm.confirm = function(notification, response) {
        NotificationsService.sendNotificationResponse(notification, response);
    }
}
