module.exports = function(config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        files: [
            '../bower_components/angular/angular.min.js',
            '../bower_components/angular-ui-router/release/angular-ui-router.min.js',
            '../node_modules/angular-mocks/angular-mocks.js',
            '../node_modules/faker/build/build/faker.js',
            '../src/*.js',
            '*spec.js'
        ],
        exclude: [],
        preprocessors: {},
        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['PhantomJS'],
        singleRun: false
    })
};
