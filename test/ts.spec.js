describe('ts application', function() {

    beforeEach(module('tsApp'));

    var $controller, $rootScope, service;

    function createNotifications(notifications, count, options) {

        var result = generateNotifications(count, options);

        result.forEach(function(notification) {
            service.addNotification(notification);
        });
        addClass(result);

        if(notifications.length) {
            result = notifications.concat(result);
        }

        return result;
    }

    function generateNotifications(count, options) {
        var categories = ["info", "warning", "error"],
            types = ["note", "ok_confirm", "ok_cancel_confirm"],
            i,
            notificationsArray = [];

        for(i = 0; i < count; i++) {
            notificationsArray.push({
                id: faker.random.number(),
                header: faker.name.findName(),
                from: faker.name.findName(),
                content: faker.lorem.sentence(),
                category: faker.random.arrayElement(categories),
                type: (options && options.type) || faker.random.arrayElement(types)
            });
        }
        return notificationsArray;
    }

    function addClass(notifications) {
        var categories = {
            info: "alert-info",
            warning: "alert-warning",
            error: "alert-danger"
        };

        notifications.forEach(function(notification) {
            notification.klass = categories[notification.category];
        });
    }

    beforeEach(inject(function(_$controller_, _$rootScope_, NotificationsService) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        service = NotificationsService;
    }));

    //
    //describe('NewNotificationController', function() {
    //    it('create new notification', function() {
    //        var controller = $controller('NewNotificationController', {$scope: $rootScope.$new()});
    //
    //        expect(controller.categories).toBeDefined();
    //    });
    //});

    describe('NotificationsService', function() {

        it('creates notifications', function() {
            var notifications = [];
            notifications = createNotifications(notifications, 1);
            expect(service.getNotificationsSize()).toBe(1);
            createNotifications(notifications, 2);
            expect(service.getNotificationsSize()).toBe(3);
        });

        it('gets notifications', function() {
            var notifications = [],
                response;
            notifications = createNotifications(notifications, 4);
            response = service.getNotifications();
            expect(Object.prototype.toString.call(response.array)).toBe('[object Array]');
            // when < 5
            expect(service.getNotificationsSize()).toBe(4);
            expect(notifications).toEqual(response.array);
            // when == 5
            notifications = createNotifications(notifications, 1);
            expect(service.getNotificationsSize()).toBe(5);
            response = service.getNotifications();
            expect(response.array).toEqual(notifications);
            // when > 5 (last 4)
            notifications = createNotifications(notifications, 2);
            expect(service.getNotificationsSize()).toBe(7);
            response = service.getNotifications();
            expect(response.array).toEqual(notifications.slice(-4));

        });

        it('removes notification', function() {
            var notifications = [];
            notifications = createNotifications(notifications, 1);
            expect(service.getNotificationsSize()).toBe(1);
            service.closeNotification(notifications[0].id);
            expect(service.getNotificationsSize()).toBe(0);
        });

        it('should have flag equals true for more then 5 notifications in queue', function() {
            var notifications = [];
            notifications = createNotifications(notifications, 4);
            expect(service.getNotifications().sliceFlag).toBeFalsy();
            notifications = createNotifications(notifications, 2);
            expect(service.getNotifications().sliceFlag).toBeTruthy();
            service.closeNotification(notifications[0].id);
            expect(service.getNotifications().sliceFlag).toBeFalsy();
        });

        it('loads notifications from external resource', inject(function($httpBackend) {
            var notifications = generateNotifications(3),
                response;
            addClass(notifications);

            $httpBackend.when('GET', 'http://www.json-generator.com/api/json/get/cgnqJBpIpu')
                .respond(notifications);

            service.loadNotifications();
            $httpBackend.flush();

            response = service.getNotifications();
            expect(service.getNotificationsSize()).toBe(3);
            expect(response.array).toEqual(notifications);
        }));

        it('send response for notifications with confirmations', inject(function($httpBackend) {
            var notifications = [],
                responseCancel, responseConfirm;
            var options1 = {type: "ok_cancel_confirm"},
                options2 = {type: "ok_confirm"};

            notifications = createNotifications(notifications, 1, options1);
            notifications = createNotifications(notifications, 1, options2);

            responseCancel = {
                id: notifications[0].id,
                from: notifications[0].from,
                result: 0
            };
            responseConfirm = {
                id: notifications[1].id,
                from: notifications[1].from,
                result: 1
            };

            expect(service.getNotificationsSize()).toBe(2);

            service.sendNotificationResponse(notifications[0], responseCancel.result);
            $httpBackend.expectPOST('/api/notification/confirm', responseCancel).respond(201, '');
            $httpBackend.flush();

            expect(service.getNotificationsSize()).toBe(1);

            service.sendNotificationResponse(notifications[1], responseConfirm.result);
            $httpBackend.expectPOST('/api/notification/confirm', responseConfirm).respond(201, '');
            $httpBackend.flush();

            expect(service.getNotificationsSize()).toBe(0);
        }));
    });
});
